# Prerequisite 
Install JFrog cli tool
https://jfrog.com/getcli/

# JFrog installation
https://jfrog.com/help/r/jfrog-installation-setup-documentation/installing-artifactory

# Maven demo
[Maven demo](/maven-examples/maven-example/README.md)

# Gradle demo
[Gradle demo](/gradle-examples/gradle-example/README.md)

# Docker demo
[Docker demo](/docker-oci-examples/docker-example/README.md)