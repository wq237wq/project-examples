# Maven demo
## Prerequiste
- JDK 1.8
  ```dtd
  java -version
  openjdk version "1.8.0_392-internal"
  OpenJDK Runtime Environment (build 1.8.0_392-internal-b08)
  OpenJDK 64-Bit Server VM (build 25.392-b08, mixed mode)
    ```
- Maven 3.6+
> Make sure you are using jdk 1.8 or above

## Set up JFrog CLI
Install JFrog cli tool
https://jfrog.com/getcli/

## Repository creation
- Create local, remote virtual maven repository 
- Set local repository as default deploy repository of virtual repository
## Set up settings.xml
- Go to Artifactory->artifacts tab, select the maven virtual repository, click "Set me up".
![img_1.png](img_1.png)
- copy the settings.xml file into ~/.m2/settings.xml

## Maven deploy

- Go to Deploy tab and copy the code snippet, paste into the bottom part of the file
```
/project-examples/maven-examples/maven-example/pom.xml
```
![img_2.png](img_2.png)

Run maven deploy
```dtd
 jf mvn deploy --build-name alex-maven-build --build-number 1
```
![img_11.png](img_11.png)
You can see the packages are upload into Artifactory server.

## Publish build info
```dtd
jf rt bp alex-maven-build 1
        16:35:28 [🔵Info] Deploying build info...
        16:35:30 [🔵Info] Build info successfully deployed.
        {
        "buildInfoUiUrl": "https://**.jfrog.io/ui/builds/alex-maven-build/1/1710836514103/published?buildRepo=artifactory-build-info"
        }

```
![img_10.png](img_10.png)
## Set metadata
jf rt sp "https://soleng.jfrog.io/artifactory/alex-maven-local/org/jfrog/test/multi3/3.7/multi3-3.7.war" "approvedBy=Alex"

## Release Life Cycle Management, create release bundle v2 from this build
```dtd
jf rbc --builds=./releaseBundle.json --signing-key=RBv2 Alex-Maven-RB 1.0.0
```
![img_12.png](img_12.png)

## Promote this release bundle to production
```dtd
 jf rbp --signing-key=RBv2 Alex-Maven-RB 1.0.0 PROD
{
  "repository_key": "release-bundles-v2",
  "release_bundle_name": "Alex-Maven-RB",
  "release_bundle_version": "1.0.0",
  "environment": "PROD",
  "included_repository_keys": [
    "payment-maven-prod-local"
  ],
  "created": "2024-03-19T09:18:22.454Z",
  "created_millis": 1710839902454
}
```            

![img_13.png](img_13.png)

# Index repository in Xray 
![img_4.png](img_4.png)
# Enable JAS scanning
![img_5.png](img_5.png)

# Check scan result in scan list
![img_6.png](img_6.png)

# Adding vulnerable dependency log4j in pom file: project-examples/maven-examples/maven-example/pom.xml
```dtd
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-core</artifactId>
            <version>2.9.1</version>
        </dependency>
```

# Repackage and scan, view the JAS results:
![img_7.png](img_7.png)

# Upgrade the dependency version to do the security fix.
```dtd
		<dependency>
			<groupId>org.apache.logging.log4j</groupId>
			<artifactId>log4j-core</artifactId>
			<version>2.20.0</version>
		</dependency>
```

# Verify the log4j issue was fix
![img_8.png](img_8.png)